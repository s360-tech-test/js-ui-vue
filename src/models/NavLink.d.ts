export interface NavLink {
  title: string
  caption: string
  route: object
  icon?: string
}
