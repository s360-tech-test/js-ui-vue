import { RouteRecordRaw } from 'vue-router'

const CatRoutes: RouteRecordRaw = {
  path: '/cats',

  component: () => import('layouts/MainLayout.vue'),

  children: [
    {
      path: '',
      component: () => import('pages/cats/IndexPage.vue'),
      name: 'CatsIndex'
    },
    {
      path: 'create',
      component: () => import('pages/cats/CreatePage.vue'),
      name: 'CatsCreate'
    },
    {
      path: ':id/edit',
      component: () => import('pages/cats/EditPage.vue'),
      name: 'CatsEdit'
    }
  ]
}

export default CatRoutes
