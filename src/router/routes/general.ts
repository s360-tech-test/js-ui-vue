import { RouteRecordRaw } from 'vue-router'

const GeneralRoutes: RouteRecordRaw = {
  path: '/',

  component: () => import('layouts/MainLayout.vue'),

  children: [
    {
      path: '',
      component: () => import('pages/general/IndexPage.vue'),
      name: 'GeneralIndex'
    }
  ]
}

export default GeneralRoutes
